#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
import tkinter as tk
import tkinter.filedialog
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import anfis
import abalone_common as common

def bell(x, c, r, s):
    return 1 / (1 + (((x - c) / r) ** 2) ** s)

class Window(tk.Frame):
    def __init__(self):
        super().__init__()
        self.pack(fill='both', expand=True)
        self.master.protocol('WM_DELETE_WINDOW', exit)

        self.plots_frame = tk.Frame(self)
        self.plots_frame.pack(fill='both', expand=True)

        self.plots_canvas = tk.Canvas(self.plots_frame)
        self.plots_canvas.grid(row=0, column=0, sticky='ENWS')

        self.figure = plt.figure(figsize=(14, 8), dpi=100)
        self.figure_canvas = FigureCanvasTkAgg(self.figure, master=self.plots_canvas)
        
        self.plots_canvas.create_window((0, 0), window=self.figure_canvas.get_tk_widget())
        def configure(event):
            self.plots_canvas.configure(scrollregion=self.plots_canvas.bbox('all'))
        self.figure_canvas.get_tk_widget().bind('<Configure>', configure)

        self.plots_v_scrollbar = tk.Scrollbar(self.plots_frame, orient='vertical',
                                                                command=self.plots_canvas.yview)
        self.plots_v_scrollbar.grid(row=0, column=1, sticky='NS')
        self.plots_frame.grid_columnconfigure(0, weight=1)
        self.plots_canvas.config(yscrollcommand=self.plots_v_scrollbar.set)

        self.plots_h_scrollbar = tk.Scrollbar(self.plots_frame, orient='horizontal',
                                                                command=self.plots_canvas.xview)
        self.plots_h_scrollbar.grid(row=1, column=0, sticky='EW')
        self.plots_frame.grid_rowconfigure(0, weight=1)
        self.plots_canvas.config(xscrollcommand=self.plots_h_scrollbar.set)

        self.meta_status_label = tk.Label(self)
        self.meta_status_label.pack(side='left')

        self.mean_abs_error_label = tk.Label(self, text='Mean absolute error: ')
        self.mean_abs_error_label.pack(side='left')

        self.mean_abs_error_var = tk.StringVar()
        self.mean_abs_error_entry = tk.Entry(self, textvariable=self.mean_abs_error_var, 
                                                   state='readonly')
        self.mean_abs_error_entry.pack(side='left')

        self.status_label = tk.Label(self)
        self.status_label.pack(side='left')

        self.reset_button = tk.Button(self, text='Reset', state='disabled',
                                            command=self.reset)
        self.reset_button.pack(side='right')

        def fit():
            path = tkinter.filedialog.askopenfilename(filetypes=[
                ('Abalone samples', '*.csv'),
            ])
            if not path:
                return

            samples = common.load_samples(path)
            self.train_samples = common.binarize_sample_labels(samples)
            self.test_input, self.test_output = \
                self.train_input, self.train_output = \
                    common.split_input_output(self.train_samples)

            if self.reset_button['state'] != 'normal':
                self.reset_button.config(state='normal')
                self.evaluate_button.config(state='normal')
                self.reset()

            self.fit()

        self.fit_button = tk.Button(self, text='Fit',
                                          command=fit)
        self.fit_button.pack(side='right')

        def evaluate():
            path = tkinter.filedialog.askopenfilename(filetypes=[
                ('Abalone samples', '*.csv'),
            ])
            if not path:
                return

            samples = common.load_samples(path)
            samples = common.binarize_sample_labels(samples)
            self.test_input, self.test_output = common.split_input_output(samples)

            self.evaluate()

        self.evaluate_button = tk.Button(self, text='Evaluate', state='disabled',
                                               command=evaluate)
        self.evaluate_button.pack(side='right')

    def reset(self):
        self.anfis = anfis.Anfis(n_rules=6)
        self.anfis.fit_start(self.train_input, self.train_output)

        self.plots = np.empty((self.anfis.n_rules, self.anfis.n_features_), object)
        self.figure.clf()
        subplots = self.figure.subplots((self.anfis.n_features_ + 1) // 2, 2)
        for i_feature in range(self.anfis.n_features_):
            axes = subplots[i_feature // 2, i_feature % 2]
            axes.set_title(self.train_samples.dtype.names[i_feature])
            values = self.train_input[:, i_feature]
            n_values = values.shape[0]
            min_, max_ = np.min(values), np.max(values)
            grid = np.linspace(min_, max_, 100)
            weights = np.full(n_values, 1 / np.max(np.histogram(values, len(grid))[0]))
            axes.hist(values, len(grid), weights=weights)
            for i_rule in range(self.anfis.n_rules):
                y = bell(grid, self.anfis.center_[i_rule, i_feature],
                               self.anfis.radius_[i_rule, i_feature],
                               self.anfis.slope_[i_rule, i_feature])
                self.plots[i_rule, i_feature] = axes.plot(grid, y)[0]

        self.figure.tight_layout()
        self.figure.subplots_adjust(wspace=0.07, hspace=0.33)
        self.figure_canvas.show()

        self.update_mean_abs_error()

        self.status_label.config(text='')

    def fit(self):
        def callback(error):
            for i_feature in range(self.anfis.n_features_):
                for i_rule in range(self.anfis.n_rules):
                    line = self.plots[i_rule, i_feature]
                    grid = line.get_data(orig=True)[0]
                    y = bell(grid, self.anfis.center_[i_rule, i_feature],
                                   self.anfis.radius_[i_rule, i_feature],
                                   self.anfis.slope_[i_rule, i_feature])
                    line.set_data(grid, y)
            self.figure_canvas.show()

            self.update_mean_abs_error()

            self.update()

        self.status_label.config(text='Fitting...')
        self.update()
        self.anfis.fit(self.train_input, self.train_output,
                       start=False, callback=callback)
        self.status_label.config(text='Done!')

    def update_mean_abs_error(self, mean_abs_error=None):
        if mean_abs_error is None:
            predicted_test_output = self.anfis.predict(self.test_input)
            mean_abs_error = \
                mean_absolute_error(self.test_output, predicted_test_output)
        self.mean_abs_error = mean_abs_error
        self.mean_abs_error_var.set(str(round(self.mean_abs_error, 4)) + ' ring(s)')

    def evaluate(self):
        self.status_label.config(text='Evaluating...')
        self.update()
        self.update_mean_abs_error()
        self.status_label.config(text='Done!')

if __name__ == '__main__':
    window = Window()
    window.mainloop()

