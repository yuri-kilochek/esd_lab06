# encoding: utf-8

import csv
import numpy as np
import numpy.lib.recfunctions

def load_samples(path):
    with open(path) as file:
        reader = csv.reader(file)
        samples = list(reader)

    return np.rec.array(samples, [
        ('sex', 'a1'),
        ('length', float),
        ('diameter', float),
        ('height', float),
        ('whole_weight', float),
        ('shucked_weight', float),
        ('viscera_weight', float),
        ('shell_weight', float),
        ('rings', float),
    ])

def binarize_sample_labels(samples):
    return np.lib.recfunctions.merge_arrays([
        np.rec.array((samples.sex == b'I').astype([('is_infant', float)])),
        np.rec.array((samples.sex == b'F').astype([('is_female', float)])),
        np.rec.array((samples.sex == b'M').astype([('is_male', float)])),
        np.lib.recfunctions.drop_fields(samples, 'sex'),
    ], flatten=True)

def split_input_output(samples):
    x = np.column_stack(samples[n] for n in samples.dtype.names if n != 'rings')
    y = samples['rings']
    return x, y
