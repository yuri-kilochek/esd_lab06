#!/usr/bin/env python
# encoding: utf-8

import os
from sklearn.model_selection import train_test_split

import abalone

window = abalone.Window()

window.fit_button.config(state='disabled')

path = os.path.join(os.path.realpath(os.path.dirname(__file__)), 'abalone.csv')
samples = abalone.common.load_samples(path)
samples = abalone.common.binarize_sample_labels(samples)
all_input, all_output = abalone.common.split_input_output(samples)

max_iters = 5
iters = 0
mean_abs_error = 0

def loop():
    global max_iters, iters, mean_abs_error

    window.meta_status_label.config(text=f'TESTING PASS #{iters + 1}/{max_iters}... ')

    window.train_samples, _, \
    window.train_input, window.test_input, \
    window.train_output, window.test_output = \
        train_test_split(samples, all_input, all_output, test_size=(1 / max_iters))

    if iters == 0:
        window.reset()
    window.fit()
    window.evaluate()

    mean_abs_error += window.mean_abs_error
    iters += 1

    if iters == max_iters:
        mean_abs_error /= iters
        window.meta_status_label.config(text=f'TESTING DONE AFTER {iters} PASS(ES)! ')
        window.update_mean_abs_error(mean_abs_error)
    else:
        window.reset()
        window.after(0, loop)

    window.update()
window.after(0, loop)

window.mainloop()
